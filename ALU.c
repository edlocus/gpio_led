#include "stdint.h"

extern uint16_t var1;

uint16_t add(uint16_t a, uint16_t b)  // function can return only one return value
{
    uint16_t sum;
    sum = a + b;
    return sum;
    return a;  // not possible
}

uint16_t sub(uint16_t data1, uint16_t data2)
{
    uint16_t result;
    result = data1 - data2;
    return result;
}

uint16_t mul(uint8_t data1, uint8_t data2)
{
    uint16_t result;
    result = data1 * data2;
    return result;
}

uint16_t div(uint16_t data1, uint16_t data2)
{
    uint16_t result;
    result = data1 / data2;
    return result;
}

uint16_t mod(uint16_t data1, uint16_t data2)
{
    uint16_t result;
    result = data1 % data2;
    return result;
}

uint8_t set_bit(uint8_t Data, uint8_t mask_bit)
{
    Data = Data  | (1<<mask_bit);
    return Data;
}

uint8_t toggle_bit(uint8_t Data, uint8_t mask_bit)
{
    Data = Data  ^ (1<<mask_bit);
    return Data;
}

uint8_t clear_bit(uint8_t Data, uint8_t mask_bit)
{
    Data = Data  & ~(1<<mask_bit);
    return Data;
}
