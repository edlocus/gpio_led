################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c123gh6pm.cmd 

C_SRCS += \
../ALU.c \
../main.c \
../tm4c123gh6pm_startup_ccs.c 

C_DEPS += \
./ALU.d \
./main.d \
./tm4c123gh6pm_startup_ccs.d 

OBJS += \
./ALU.obj \
./main.obj \
./tm4c123gh6pm_startup_ccs.obj 

OBJS__QUOTED += \
"ALU.obj" \
"main.obj" \
"tm4c123gh6pm_startup_ccs.obj" 

C_DEPS__QUOTED += \
"ALU.d" \
"main.d" \
"tm4c123gh6pm_startup_ccs.d" 

C_SRCS__QUOTED += \
"../ALU.c" \
"../main.c" \
"../tm4c123gh6pm_startup_ccs.c" 


