#include "stdint.h"

#ifndef __ALU__
#define __ALU__

uint16_t add (uint16_t a, uint16_t b);
uint16_t sub(uint16_t data1, uint16_t data2);
uint16_t mul(uint16_t data1, uint16_t data2);
uint16_t div(uint16_t data1, uint16_t data2);
uint16_t mod(uint16_t data1, uint16_t data2);


uint8_t set_bit(uint8_t Data, uint8_t mask_bit);
uint8_t clear_bit(uint8_t Data, uint8_t mask_bit);
uint8_t toggle_bit(uint8_t Data, uint8_t mask_bit);

#endif
