#include "TM4C123GH6PM.h"
#include "ALU.h"
#include "stdint.h"

#define BIT0    0x01
#define BIT1    0x02
#define BIT2    0x04
#define BIT3    0x08
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80

#define RED_LED     BIT1
#define BLUE_LED    BIT2
#define GREEN_LED   BIT3

#define GPIOCR_PORT_F  (*((volatile unsigned long *)0x40025524))

void delay(long delay_time)
{
    long i;
    for(i=0;i<delay_time;i++)
    {

    }
}
/**
 * main.c
 */
int main(void)
{
    /*** GPIO Clock gating 5th bit to enable port F ***/
    SYSCTL->RCGC2 |= BIT5; // 1<<5  == 32/0x20


    GPIOF->LOCK |= 0x4C4F434B; //0x4C4F434B
    //GPIOCR_PORT_F |= 0x1F;
    GPIOF->CR |= BIT0;


    /*** GPIO Digital enable to enable digital function   ***/
    GPIOF->DEN |= 0x0F;

    /*** portF 0th bit as GPIO INPUT  ***/
    GPIOF->DIR &= ~BIT0;

    /*** GPIO Direction register 0 - input/ 1 - output ***/
    GPIOF->DIR |= BIT1 + BIT2 + BIT3;

    /*** Enable pull up resistor fot portF 0 ***/
    GPIOF->PUR |= BIT0;

   while(1)
   {
       /*
        * Blinking LED
        *
        *   */

       GPIOF->DATA |= RED_LED;
       delay(1000000);
       GPIOF->DATA &= ~RED_LED;
       delay(1000000);


/*
 * GPIO INPUT APPLICATION
 *
       if(!(GPIOF->DATA & BIT0))
           GPIOF->DATA |= RED_LED;
       else
           GPIOF->DATA &= ~(RED_LED);
*/

   }

}
